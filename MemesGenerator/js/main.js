// Your code here!
var app = angular.module('app', []);

app.controller('defaultCtrl', ['$scope', '$http', function ($scope, $http) {


    //Pobieranie zdjęc memów
    $http({
        method: 'get',
        url: 'https://ronreiter-meme-generator.p.mashape.com/fonts',
        headers: { 'X-Mashape-Key': 'ayruhyyT8dmsh3B9oOJdAzmYIEFZp1rqHA3jsnh1xY72UrNdkp' },
        "content-type": "application/json",
        "Accept": "text/plain"
    }).success(function (data) {
       // console.log('Udało się pobrać : ' + data);
        $scope.memesFonts = data;
    });

    $http({
        method: 'get',
        url: 'https://ronreiter-meme-generator.p.mashape.com/images',
        headers: { 'X-Mashape-Key': 'ayruhyyT8dmsh3B9oOJdAzmYIEFZp1rqHA3jsnh1xY72UrNdkp',
                 'responseType': "image/jpeg"},
        "content-type": "application/json",
        "Accept": "text/plain"
    }).success(function (data) {
       // console.log('Udało się pobrać : ' + data);
        $scope.memesImgs = data;
    });

  
  
    $scope.createM = function(){
        $('.generatedMem').css("display", "block");
        $http({
          method: 'get',
          url: 'https://ronreiter-meme-generator.p.mashape.com/meme?bottom=' + $scope.bottomTxt + '&font=' + $scope.fType + '&font_size=' + $scope.fSize + '&meme=' + $scope.imgType + '&top=' + $scope.topTxt,
          headers: { 'X-Mashape-Key': 'ayruhyyT8dmsh3B9oOJdAzmYIEFZp1rqHA3jsnh1xY72UrNdkp'},
          responseType: 'blob',
        }).success(function(data){
          //$scope.memImage = data;
         
          document.getElementById('mem').src=window.URL.createObjectURL(data);
          document.getElementById('downLink').href=window.URL.createObjectURL(data);
          
        });

    };
  
  
}]);